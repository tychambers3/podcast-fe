import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import IndexPage from './pages/IndexPage/IndexPage';
import './global.scss';
import { Provider } from 'mobx-react';

import SignupPage from './pages/SignupPage/SignupPage';
import DashboardPage from './pages/DashboardPage/DashboardPage';
import CreatePodcastPage from './pages/CreatePodcastPage/CreatePodcastPage';

import UserStore from './stores/UserStore';
import PodcastStore from './stores/PodcastStore';

import Axios from 'axios';

const stores = {
  UserStore,
  PodcastStore
}

const initState = {
  loading: true
}

const getUserOnLoad = async (store) => {
  Axios.get(`${process.env.REACT_APP_DATABASE_URL}/api/auth`, {
    headers: {
      token: store.getToken()
    }
  })
  .then(res => {
    store.setUser(res.data);
    initState.loading = false;
  })
  .catch(err => console.log(err));
}


const PrivateRoute = ({ component: Component, ...rest }) => {
  const token = window.localStorage.getItem('jwt');

  
  return (
    <Route
      {...rest}
      render={props =>
        token ? (
          <Component {...props} />
        ) : (
          <React.Fragment>
            <Redirect
              to={{
                pathname: "/signup",
                state: { from: props.location }
              }}
            />
          </React.Fragment>
          
        )
      }
    />
  );
}

ReactDOM.render(
  <Provider {...stores}>
    <Router basename="/">
      <Switch>
        <Route exact path="/" component={IndexPage}/>
        <Route path="/signup" component={SignupPage}/>
        <PrivateRoute path="/dashboard/:subPage" component={DashboardPage} />
        <PrivateRoute path="/podcast/new" component={CreatePodcastPage} />
      </Switch>
    </Router>
  </Provider>, document.getElementById('root'), () => getUserOnLoad(stores.UserStore));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
