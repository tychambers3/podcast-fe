import React, { Component } from 'react'
import './DashNavbar.scss';
import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import CurrentPodcastWidget from '../../components/CurrentPodcastWidget/CurrentPodcastWidget';

class Sidebar extends Component {
  render() {
    return (
      <div className="dash-sidebar jc-sb">

        {this.props.UserStore.user.podcasts === null && 
          <p>Create a podcast</p>
        }
        
        {this.props.UserStore.user.podcasts !== null && 
          <CurrentPodcastWidget />
        }

        <ul className="sidebar-list d-f fx-1 ai-c jc-fe">
          <li>
            <Link to="/dashboard/home" className="sidebar-link">
              <i className="fas fa-home"></i>
              Home
            </Link>
          </li>

          <li>
            <Link to="#" className="sidebar-link">
              <i className="fas fa-play-circle"></i>
              Episodes
            </Link>
          </li>

          <li>
            <Link to="#" className="sidebar-link">
              <i className="fas fa-cog"></i>
              Podcast Settings
            </Link>
          </li>

          <li>
            <Link to="#" className="sidebar-link">
              <i className="fas fa-chart-pie"></i>
              Statistics
            </Link>
          </li>

          <li>
            <Link to="#" className="sidebar-link">
              <i className="fas fa-globe-americas"></i>
              Website
            </Link>
          </li>

          <li>
            <Link to="#" className="sidebar-link">
              <i className="fas fa-microphone"></i>
              Studio
            </Link>
          </li>

          <li className="current-user">
            <Link to="#" className="sidebar-link">
              Tyrel Chambers
            </Link>
          </li>
        </ul>

        
      </div>
    )
  }
}

const DashNavbar = inject('UserStore')(observer(Sidebar));

export default DashNavbar;