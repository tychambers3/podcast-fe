import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './Navbar.scss';
import { inject, observer } from 'mobx-react';

class Nav extends Component {
  render() {
    return (
      <nav className="navbar">
        <ul className="d-f ai-c">
          <li className="mr+ navbar-item">
            <Link to="/">
              Home
            </Link>
          </li>

          <li className="mr+ navbar-item">
            <Link to="/about">
              About
            </Link>
          </li>

          <li className="mr+ navbar-item">
            <Link to="/">
              Pricing
            </Link>
          </li>

          {this.props.UserStore.user.email && 
            <li className="mr+ navbar-item">
              <Link to="/dashboard/home">
                My Dashboard
              </Link>
            </li>
          }


          {!this.props.UserStore.user.email && 
            <li className="mr+ navbar-item">
              <Link to="/signup">
                Signup
              </Link>
            </li>
          }
        </ul>
      </nav>
    )
  }
}

const Navbar = inject('UserStore')(observer(Nav));

export default Navbar;