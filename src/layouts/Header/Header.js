import React, { Component } from 'react'
import Navbar from '../Navbar/Navbar';
import './Header.scss';

export default class Header extends Component {
  render() {
    return (
      <header className=" pt-- pr- pb-- pl-  header">
        <div className=" d-f jc-sb ai-c w-1056px ml-a mr-a">
          <div className="brand">

          </div>

          <Navbar/>
        </div>
      </header>
    )
  }
}
