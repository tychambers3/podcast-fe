import { observable, action, decorate, reaction } from 'mobx';

class UserStore {
  constructor() {
    reaction(
      () => this.user,
      user => window.sessionStorage.setItem('user', JSON.stringify(user))
    )
  }
  user = {} || JSON.parse(window.sessionStorage.getItem('user'));

  setUser(user) {
    this.user = user;
  }

  getUser() {
    return JSON.parse(window.sessionStorage.getItem('user'));
  }
  setToken(token) {
    this.token = window.localStorage.setItem('jwt', token);
  }

  getToken() {
    return window.localStorage.getItem('jwt');
  }
}

decorate(UserStore, {
  user: observable,
  setUser: action,
  token: observable,
  setToken: action
});

export default new UserStore();