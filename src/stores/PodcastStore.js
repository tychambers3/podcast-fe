import { action, observable, decorate } from 'mobx';

class PodcastStore {
  podcastOnDashboard = {};

  setPodcastOnDashboard(podcast) {
    this.podcastOnDashboard = {...podcast};
  }

  getPodcastOnDashboard() {
    return this.podcastOnDashboard;
  }
}

decorate(PodcastStore, {
  podcastOnDashboard: observable
});

export default new PodcastStore();