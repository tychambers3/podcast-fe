import React, { Component } from 'react'
import Axios from 'axios';
import { inject, observer } from 'mobx-react';
import './CurrentPodcastWidget.scss';
class PodcastWidget extends Component {
  constructor() {
    super();
    this.state = {
      podcasts: [],
      loading: true,
      currentPodcast: 0
    }
  }
  componentDidMount() {
    const user = this.props.UserStore.getUser();
    this.getCurrentPodcast(user.podcasts[0]._id);
    this.setState({podcasts: [...user.podcasts], loading: false});
  } 

  activeHandler = e => {
    const elems = document.querySelectorAll('[data-podcast]');
    let elemArray = [];
    const li = e.target.closest('.current-podcast-widget-dropdown-item').getAttribute('data-podcast');
    const podcastId = e.target.closest('.current-podcast-widget-dropdown-item').getAttribute('data-podcast-alt');

    for ( let i = 0; i < elems.length; i++ ) {
      elemArray.push(elems[i]);
    }

    elemArray.forEach(x => x.classList.remove('current-podcast-widget-active'));

    elemArray[li].classList.add('current-podcast-widget-active');

    this.setState({currentPodcast: li}, () => this.getCurrentPodcast(podcastId));
  }

  getCurrentPodcast = (id) => {
    Axios.get(`${process.env.REACT_APP_DATABASE_URL}/api/podcasts/${this.props.UserStore.getUser()._id}/podcasts/${id}`, 
    {
      headers: {
        'token': this.props.UserStore.getToken()
      }
    })
    .then(res => this.props.PodcastStore.setPodcastOnDashboard(res.data))
    .catch(err => console.log(err));
  }
  
  render() {
    if ( this.state.loading ) return null;

    return (
      <div className="current-podcast-widget d-f ai-c m-">
        <div className="current-podcast-widget-logo mr--">
        </div>

        <div className="d-f fxd-c current-podcast-widget-details ">
          <h5 className="m0 mb---">{this.state.podcasts[this.state.currentPodcast].title}</h5>
          <p className="m0">{this.state.podcasts[this.state.currentPodcast].episodes.length} Episodes</p>
        </div>
        <i className="fas fa-chevron-down ml+"></i>

        <ul className="dropdown">
          {this.state.podcasts.map((x, id) => {
            return (
              <li key={id} className="d-f ai-c current-podcast-widget-dropdown-item" data-podcast-alt={x._id} data-podcast={id} onClick={this.activeHandler}>
                <div className="current-podcast-widget-logo mr--"></div>
                <div className="d-f fxd-c  ">
                  <h3 className="m0 mb--- font-primary">{x.title}</h3>
                  <p className="m0 font-tiertiary">{x.episodes.length} Episodes</p>
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    )
  }
}

const CurrentPodcastWidget = inject('UserStore', 'PodcastStore')(observer(PodcastWidget));

export default CurrentPodcastWidget;