import React from 'react';
import Axios from 'axios';
import { inject, observer } from 'mobx-react';

class Form extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: ""
    }
  }

  inputHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  submitHandler = (e) => {
    e.preventDefault();
    const { email, password } = this.state;

    Axios.post(`${process.env.REACT_APP_DATABASE_URL}/api/auth/signup`, {
      email,
      password
    })
    .then(res=> {
      this.props.UserStore.setUser(res.data.user);
      this.props.UserStore.setToken(res.data.token);
      window.location.pathname = '/podcast/new';
    })
    .catch(err => console.log(err));
  }

  render() {
    return (
      <form className="form form-bg">
        <div className="field-group">
          <label htmlFor="email" className="label">Email</label>
          <input type="email" className="input" placeholder="user@example.com" name="email" onChange={(e) => this.inputHandler(e)}/>
        </div>

        <div className="field-group">
          <label htmlFor="password" className="label">Password</label>
          <input type="password" className="input" placeholder="Not 'password' or '123' please" name="password" onChange={(e) => this.inputHandler(e)}/>
        </div>

        <div className="field-group">
          <label htmlFor="confirmPassword" className="label">Confirm Password</label>
          <input type="password" className="input" placeholder="Rewrite your password"/>
        </div>

        <div className="field-group">
          <button className="btn primary full-width" onClick={(e) => this.submitHandler(e)}>Sign Up!</button>
        </div>
      </form>
    );
  }
}

const SignupForm = inject('UserStore')(observer(Form));

export default SignupForm;