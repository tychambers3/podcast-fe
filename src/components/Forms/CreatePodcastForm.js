import React, { Component } from 'react'
import PodcastLogoUpload from '../PodcastLogoUpload/PodcastLogoUpload';
import './CreatePodcastForm.scss';
import Axios from 'axios';
import { inject, observer } from 'mobx-react';
import { Redirect } from 'react-router-dom';

class PodcastForm extends Component {
  constructor() {
    super();
    this.state = {
      contributors: [],
      title: "",
      description: "",
      redirect: false
    }
  }
  
  componentDidMount() {
    
  }

  submitHandler = e => {
    e.preventDefault();

    const { title, description } = this.state;

    Axios.post(`${process.env.REACT_APP_DATABASE_URL}/api/podcasts/new`, {
      title,
      description
    }, {
      headers: {
        'token': this.props.UserStore.getToken()
      }
    })
    .then(res => {
      this.setState({redirect: true});
    })
    .catch(err => console.log(err));
  }

  addContributor = (e) => {
    e.preventDefault();
   
    const input = document.querySelector('#contributors').value;
    this.setState({contributors: [...this.state.contributors, input]});
  }

  inputHandler = e => {
    this.setState({[e.target.name]: e.target.value});
  }

  render() {

    if (this.state.redirect) return <Redirect to="/dashboard/home" />;

    return (
      <form className="form">
        <PodcastLogoUpload />
        <p className="subtle mb+">Images of 3000 x 3000 are recommended</p>
        <div className="field-group">
          <label htmlFor="title" className="label">Podcast Title</label>
          <input type="text" className="input" placeholder="What's the name of your podcast?" name="title" onChange={e => this.inputHandler(e)}/>
        </div>

        <div className="field-group">
          <label htmlFor="description" className="label">Description</label>
          <input type="text" className="input" placeholder="What's it about?" name="description" onChange={e => this.inputHandler(e)}/>
        </div>

        <div className="field-group">
          <label htmlFor="contributors" className="label">Contributors</label>
          
          <div className="d-f ai-c">
            <input type="email" className="input fx-1" placeholder="Who's involved?" name="contributors" id="contributors"/>
            <button className="add-contributor btn small ml-" onClick={(e) => this.addContributor(e)}>Send Invite</button>
          </div>
        </div>
        
        <div className="contributor-list">
          <h3 className="label">Invited</h3>

          {this.state.contributors.length === 0 &&
            <p className="subtle">No contributors</p>
          }

          {this.state.contributors.map((x, id) => {
            return (
              <p className="contributor-list-item" key={id}>{x}</p>
            );
          })}
        </div>
        
        <button className="btn primary full-width" onClick={this.submitHandler}>Create podcast</button>
      </form>
    )
  }
}

const CreatePodcastForm = inject('UserStore')(observer(PodcastForm));

export default CreatePodcastForm;