import React, { Component } from 'react'
import CreatePodcastForm from '../../components/Forms/CreatePodcastForm';
import './CreatePodcastPage.scss';
import Header from '../../layouts/Header/Header';

export default class OnboardPage extends Component {
  render() {
    return (
      <div className="h-100v">
        <Header />

        <div className="d-f fxd-c ai-c jc-c">
          <h1 className="primary-title">Let's Create A Podcast</h1>
          <CreatePodcastForm />
        </div>
      </div>
    )
  }
}
