import React, { Component } from 'react'
import { inject, observer } from 'mobx-react';
import './DashboardHome.scss';

class Home extends Component {
  
  render() {
    const podcast = this.props.PodcastStore.getPodcastOnDashboard();

    if ( !podcast._id ) return null;

    return (
      <div className="w-1056px ml-a mr-a">
        <section className="podcast-header m+">
          <h1 className="primary-title m0 mb--- tt-c">{podcast.title}</h1>
          <p className="subtle-alt m0">{podcast.episodes.length} Published Episodes</p>
        </section>
      </div>
    )
  }
}

const DashboardHome = inject('UserStore', 'PodcastStore')(observer(Home));

export default DashboardHome;