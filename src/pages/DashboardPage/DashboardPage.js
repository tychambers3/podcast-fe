import React, { Component } from 'react'
import DashboardHome from './DashboardHome/DashboardHome';
import DashNavbar from '../../layouts/DashNavbar/DashNavbar';

export default class DashboardPage extends Component {
  render() {
    const slug = this.props.match.params.subPage;

    return (
      <main className="d-f fxd-c">
        <DashNavbar />

        <section className="p-">
          <PageSwitcher subPage={slug}/>
        </section>
      </main>
    )
  }
}

const PageSwitcher = (props) => {
  const subPages = {
    "home": <DashboardHome />
  }
  return subPages[props.subPage];
}