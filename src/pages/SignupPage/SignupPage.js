import React, { Component } from 'react'
import SignupForm from '../../components/Forms/SignupForm';
import Header from '../../layouts/Header/Header';

export default class SignupPage extends Component {
  render() {
    return (
      <div className=" ml-a mr-a">
        <Header/>

        <div className="w-400px ml-a mr-a mt+">
          <h1 className="primary-title ta-c mb+">Sign up for SoundCave!</h1>
          <SignupForm /> 
        </div>
      </div>
    )
  }
}
