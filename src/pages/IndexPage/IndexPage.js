import React, { Component } from 'react'
import './IndexPage.scss';
import Header from '../../layouts/Header/Header';
import { Link } from 'react-router-dom';
export default class IndexPage extends Component {
  render() {
    return (
      <main className="">
          <Header/>
        <div className="h-100v w-1056px ml-a mr-a pt-">

          <div className="hero d-f fxd-c jc-c">
            <h1 className="super-title ta-c">SoundCave</h1>
            <p className="w-400px ta-c ml-a mr-a">
              Our platform was created with podcasters in mind. 
              We wanted to give you the place to grow your show 
              and reach new audiences.
            </p>

            <div className="w-266px d-f jc-sb ml-a mr-a mt+">
              <Link to="/signup" className="btn primary">
                Sign Up
              </Link>

              <Link to="/login" className="btn hollow">
                Login
              </Link>
            </div>
          </div>
        </div>

        <section className="section-1 h-100v">
          <p className="w-560px ta-c ml-a mr-a secondary-title">
            SoundCave is a professional podcast hosting solution made with
            podcast creators in mind. 
          </p>
          
        </section>
      </main>
    )
  }
}
